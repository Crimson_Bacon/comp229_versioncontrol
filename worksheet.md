# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
  * (B) https://github.com/Whiley/WhileyCompiler

And answer the following questions about them:

  * These repositories are at two different websites - github and bitbucket - what are these sites?  What service do they provide? Which is better?
      These sites are both sites for storing repositories as well as their version histories. There is no better or worse without context. Although it is very small difference, I do believe if developers many small projects Bitbucket is probably more preferred because of the per repository costs. Github would be preferred for bigger projects that have large teams working on them. 

  * Who made the last commit to repository A?
      The person that made the last commit on repository A is Matt Roberts on the 15th of August 2018 with a commit message of "first round of meddling by Matt".

  * Who made the first commit to repository A?
      The person that made the first commit on repository A is Jon Mountjoy on the 11th of August 2014 with a commit message of "getting started".

  * Who made the first and last commits to repository B?
      The person that made the last commit on repository B is DavePearce on the 25th of July 2019 with a commit message of "Add more test cases for templates". 
      The person that made the first commit on repository B is DavePearce on th 25th of June 2010 with a commit message of "Shuffling around."

  * Are either/both of these projects active at the moment? 🤔 If not, what do you think happened?
      Repository A has not been active since mid 2018, and began in 2014. It says in the readme that the project was an academic project. Given the timeframe that it was active for I can only guess that this was a university or college project that was dropped when they graduated. This seems quite likely as the start of the project was heavily commited by two users who could be teachers as they do not show up in the later commits. 
      Repository B is still active as the last commit happened 6 days ago.

  * 🤔 Which file in each project has had the most activity?
      

# Task 2

The repository you just cloned is a VSCode project, so lets work with it.  It currently will print "Hello World" message to the console when run.

You will find "Run" and "Debug" commands over the `main` method.  Try them out.  You can also trigger them with `F5` for "Debug" and `Ctrl-F5` for "Run"

Modify the application so that instead it prints

~~~~~
Red vs. Blue
~~~~~